package main

import (
	"github.com/dchest/uniuri"
	"github.com/gin-gonic/gin"
	"log"
	"math/rand"
	"net/http"
	"time"
)

const (
	// TokenLen - Length of authentication token generated
	TokenLen = 40
	// MaxOffers - Maximum random offers generated
	MaxOffers = 20
)

var generatedToken string
var berlin *OfferArea

func indexEndpoint(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"status": "OK"})
}

func tokenEndpoint(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"token": generatedToken})
}

func offersEndpoint(c *gin.Context) {

	if berlin == nil {
		c.JSON(http.StatusInternalServerError, gin.H{"status": "berlin area not available"})
		return
	}

	// Generate atleast 2 offers
	noOfOffers := 2 + rand.Intn(MaxOffers)
	offers := make([]OfferLocation, noOfOffers)
	for i := 0; i < noOfOffers; i++ {
		offers[i].ID = i
		offers[i].Latitude, offers[i].Longitude = berlin.GetRandomLocation()
	}

	c.JSON(http.StatusOK, offers)

}

// CORSMiddleware - Returns CORS headers
func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Content-Type", "application/json")
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
		// OPTIONS needs to be handled explicitly
		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(http.StatusOK)
		} else {
			c.Next()
		}
	}
}

// TokenAuthMiddleware checks for token field in the request
// and authenticates against the stored token
func TokenAuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Request.ParseForm()
		token := c.Request.Form.Get("token")
		// check if the token sent is same as generated token
		if token != generatedToken {
			c.JSON(http.StatusUnauthorized, gin.H{"status": "unauthorized"})
			c.Abort()
			return
		}
		c.Next()
	}
}

func basicAuth() gin.HandlerFunc {
	return gin.BasicAuth(gin.Accounts{"locafox": "LocaF#xes!"})
}

func main() {

	var err error

	rand.Seed(time.Now().Unix())

	// Define new area for offers
	berlin, err = NewArea("Berlin", 52.47, 13.24, 52.55, 13.50)
	if err != nil {
		log.Fatal(err.Error())
	}

	// generating unique 40 byte token
	generatedToken = uniuri.NewLen(TokenLen)

	r := gin.Default()

	r.Use(CORSMiddleware())

	r.GET("/", indexEndpoint)

	// Basic Authenticate all /v1 endpoints
	v1 := r.Group("/v1", basicAuth())
	v1.POST("/token", tokenEndpoint)
	// Authenticate Token before sending random offers
	v1.POST("/offers", TokenAuthMiddleware(), offersEndpoint)

	r.Run(":3000")
}
