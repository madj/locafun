package main

import (
	"errors"
	"math/rand"
)

// OfferLocation - offer's location
type OfferLocation struct {
	ID        int     `json:"id"`
	Latitude  float64 `json:"lat"`
	Longitude float64 `json:"long"`
}

// OfferArea defines the bounding box for offer area
type OfferArea struct {
	Name                             string
	minLat, minLong, maxLat, maxLong float64
	latDiff, longDiff                float64
}

// NewArea creates an instance of offer area setting the bounding box
func NewArea(name string, x1, y1, x2, y2 float64) (offerArea *OfferArea, err error) {
	offerArea = &OfferArea{Name: name}

	// validate
	if x1 > 90 || x1 < -90 || x2 > 90 || x2 < -90 {
		err = errors.New("Invalid Latitude Specified")
		return nil, err
	}
	if y1 > 180 || y1 < -180 || y2 > 180 || y2 < -180 {
		err = errors.New("Invalid Longitude Specified")
		return nil, err
	}

	// swap if required
	if x1 > x2 {
		x1, x2 = x2, x1
	}
	if y1 > y2 {
		y1, y2 = y2, y1
	}

	offerArea.minLat = x1
	offerArea.minLong = y1
	offerArea.maxLat = x2
	offerArea.maxLong = y2

	offerArea.latDiff = x2 - x1
	offerArea.longDiff = y2 - y1
	return
}

// GetRandomLocation generates random lat, long pair within the bounding box
func (oa *OfferArea) GetRandomLocation() (lat, long float64) {
	lat = oa.minLat + rand.Float64()*oa.latDiff
	long = oa.minLong + rand.Float64()*oa.longDiff
	return
}
